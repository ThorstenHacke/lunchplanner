export const createPlan = (dishes, guests) => {
    let result = Array(7);
    let workingDishes = [...dishes];

    for(let i = 0; i < 7; i++){
        const guestsOfTheDay = getGuestsOnWeekday(guests, i);
        const notParticipating = guests.filter(g => guestsOfTheDay.indexOf(g) < 0);
        let likedDishes = getDishesWithoutDislikes(workingDishes, guestsOfTheDay, notParticipating);
        if(likedDishes.length === 0){
            likedDishes = workingDishes;
        }
        const orderedDishes = getDishesOrderedByGuestRankings(likedDishes, guestsOfTheDay);
        const dishIndex = getRndInteger(0, orderedDishes.length-1);
        const selectedDish = orderedDishes[dishIndex];
        result[i] = selectedDish;
        const orderedIndex = workingDishes.indexOf(selectedDish);
        workingDishes.splice(orderedIndex, 1)
    }
    return result;
}
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
  }
export const getGuestsOnWeekday = (guests, weekday) => {
    return guests.filter(g => g.weekdays && g.weekdays[weekday]);
}
export const getDishesOrderedByGuestRankings = (dishes, guests) => {
    const sorted = [...dishes]
    sorted.sort((a, b) => compareDishes(a, b, guests));
    return sorted;
}

export const compareDishes = (dishA, dishB, guests) => {
    const ratingA = getDishTotalRank(dishA, guests);
    const ratingB = getDishTotalRank(dishB, guests);
    if (ratingA < ratingB) {
        return 1;
    }
    if (ratingA > ratingB) {
        return -1;
    }
    return 0;
}
export const getDishTotalRank = (dish, guests) => {
    const keys = Object.keys(dish.ratings).filter(key => guests.some(g => g.id == key));
    let rating = 0
    for (const key of keys) {
        rating += dish.ratings[key]
    }
    return rating;
}

export const getDishesWithoutDislikes = (dishes, guests, notParticipating) => {
    const filtered = [...dishes];
    return filtered.filter(dish => {
        const guestNames = Object.keys(dish.ratings).filter(key => {
            return guests.some(g => g.id == key);
            
        });
        const notParticipatingtNames = Object.keys(dish.ratings).filter(key => {
            return notParticipating.some(g => g.id == key)
        });
        for (const key of guestNames) {
            if(dish.ratings[key] == 0){
                return false;
            }
        }
        for (const key of notParticipatingtNames) {
            if(dish.ratings[key] != 0){
                return false;
            }
        }
        return true;
    })
}