import { createContext } from "preact";
import { openDB } from 'idb';
import { createPlan } from "../services/plancreatorservice";

export const ContextProviderContext = createContext({ data: {} });

const GUESTS = "Guests";
const DISHES = "Dishes";
const LUNCH_PLANNER_DB = "LunchPlannerDB";
const PLAN = "Plan"
export const ContextProvider = ({ children }) => {

    const getDataBase = async () => {
        const db = await openDB(LUNCH_PLANNER_DB, 5, {

            upgrade: (upgradeDB, oldversion,newversion) => {
                if(oldversion < 5){
                    upgradeDB.createObjectStore(PLAN, { autoIncrement: true, keyPath: 'id' })
                }
                if(oldversion < 4)
                {
                    upgradeDB.createObjectStore(DISHES, { autoIncrement: true, keyPath: 'id' })
                    upgradeDB.createObjectStore(GUESTS, { autoIncrement: true, keyPath: 'id' })
                }
            }
        });
        return db;
    }

    const generateNewPlan = async () => {
        const dishes = await getDishes();
        const guests = await getGuests();
        const plan = createPlan(dishes, guests);
        await savePlan(plan);
        return plan;
    }

    const importFromJson = async (newdata) => {
        const result = JSON.parse(newdata);
        const db = await getDataBase();
        db.clear(DISHES);
        for (const dish of result.dishes) {
            await db.add(DISHES, dish)
        }
        db.clear(GUESTS);
        for (const guests of result.guests) {
            await db.add(GUESTS, guests)
        }
    }

    const exportToJson = async () => {
        const db = await getDataBase();
        const result = {};
        result.dishes = await db.getAll(DISHES);
        result.guests = await db.getAll(GUESTS);
        return JSON.stringify(result);
    }
    const getDishes = async () => {
        const db = await getDataBase();
        const dishes = await db.getAll(DISHES);
        return dishes;
    }

    const getPlans = async () => {
        const db = await getDataBase();
        const plans = await db.getAll("Plan");
        return plans;
    }

    const savePlan = async (plan) => {
        const db = await getDataBase();
        await db.put("Plan", plan)

    }

    const getGuests = async () => {
        const db = await getDataBase();
        const guests = await db.getAll(GUESTS);
        return guests;
    }

    const saveDish = async (dish) => {
        const db = await getDataBase();
        db.put(DISHES, dish)
    }

    const saveGuest = async (guest) => {
        const db = await getDataBase();
        db.put(GUESTS, guest)
    }
    const deleteDish = async (id) => {
        const db = await getDataBase();
        await db.delete(DISHES, id);
    }

    const deleteGuest = async (id) => {
        const db = await getDataBase();
        await db.delete(GUESTS, id);
    }
    return (
        <ContextProviderContext.Provider
            value={{
                getDishes,
                importFromJson,
                exportToJson,
                saveDish,
                deleteDish,
                getGuests,
                saveGuest,
                deleteGuest,
                generateNewPlan,
                getPlans,
            }}
        >
            {children}
        </ContextProviderContext.Provider>
    );
};
