import { useContext } from "preact/compat";
import { ContextProviderContext } from "./contextProvider";

export const useGlobals = () => {
    const {
        getDishes,
        importFromJson,
        exportToJson,
        saveDish,
        deleteDish,
        getGuests,
        saveGuest,
        deleteGuest,
        generateNewPlan,
        getPlans,
    } = useContext(ContextProviderContext);

    return {
        getDishes,
        importFromJson,
        exportToJson,
        saveDish,
        deleteDish,
        getGuests,
        saveGuest,
        deleteGuest,
        generateNewPlan,
        getPlans,
    }
}