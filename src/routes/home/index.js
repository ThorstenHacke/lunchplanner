import { h } from 'preact';
import style from './style.css';
import { useGlobals } from '../../providers/useGlobals';
import DetailsView from '../../components/detailsview';
import { useEffect, useState } from 'preact/hooks';
import { weekday } from '../../services/helpers';

const Home = () => {

	const [plan, setPlan] = useState([]); 
	const {getPlans, generateNewPlan} = useGlobals()
	useEffect(async () => {
		const loadedPlans = await getPlans();
		const lastPlan = loadedPlans[loadedPlans.length-1];
		if(!plan || JSON.stringify(lastPlan)!=JSON.stringify(plan)){
			setPlan(lastPlan);
		}
	});

	const createNewPlan = async () => {
		const newplan = await generateNewPlan();
		if(!plan || JSON.stringify(newplan)!=JSON.stringify(plan)){
			setPlan(newplan);
		}
	}

	return <DetailsView headline="Plan" onClick={createNewPlan} buttonText="Generate Plan">
			<div class={style.gridContainer}>
		
		{plan.map((p, index) => (
			<><div class={style.gridColOne}>{weekday[index]}</div><div class={style.gridColTwo}>{p.name}</div></>
			
			))}
		</div>
		</DetailsView>

};

export default Home;
