import { h } from 'preact';
import { useGlobals } from '../../providers/useGlobals';
import ListView from '../../components/listview';


// Note: `user` comes from the URL, courtesy of our router
const Dishes = () => {
	const {getDishes, deleteDish} = useGlobals()
	return (
		<ListView deleteItem={deleteDish} detailRoute="dish" getItems={getDishes} headline={"Dishes"}></ListView>
	);
}

export default Dishes;
