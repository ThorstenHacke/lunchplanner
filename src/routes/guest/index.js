import { h } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { useGlobals } from '../../providers/useGlobals';
import style from './style.css';
import { route } from 'preact-router';
import TextInput from '../../components/textInput/textInput';
import DetailsView from '../../components/detailsview';
import { weekday } from '../../services/helpers';


const Guest = ({id}) => {
	const [guest, setGuest] = useState({name:""})
	const {saveGuest, getGuests} = useGlobals()
	useEffect(async () => {
		if(id){
			const numberId = Number.parseInt(id);
			const loadedGuestes = await getGuests();
			const found = loadedGuestes.find(curguest => curguest.id === numberId);
			if(found){
				setGuest(found);
			}
		}
	}, [id])


	const checkWeekday = (event, index) => {
		if(!guest.weekdays){
			guest.weekdays = [];
		}
		guest.weekdays[index] = event.target.checked
	}

	return (
		<DetailsView headline="Guest" onClick={() => { saveGuest(guest); route("/guests");}}>
			<TextInput label="Name" value={guest.name} onInput={(e) => {guest.name = e.target.value}} />
			<TextInput label="Description" value={guest.description} onInput={(e) => {guest.description = e.target.value}} />
			{weekday.map((value,index) => {
				const chkId = "chk"+guest.id+"_"+index;
				return (
				
				<div>
					<input class={style.weekdayCheck} type="checkbox" id={chkId} value={index} checked={(guest.weekdays? guest.weekdays[index]:false)} oninput={(e) => checkWeekday(e, index)}/>

					<label class={style.weekdayLabel} for={chkId}>{value}</label>
							
				</div>

			)
			})}
			
		</DetailsView>
	);
}

export default Guest;
