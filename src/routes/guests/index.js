import { h } from 'preact';
import { useGlobals } from '../../providers/useGlobals';
import style from './style.css';
import { Link } from 'preact-router/match';
import { useEffect, useState } from 'preact/hooks';
import ListView from '../../components/listview';


const Guests = () => {
	const {getGuests, deleteGuest} = useGlobals()
	
	return (
		<ListView deleteItem={deleteGuest} detailRoute="guest" getItems={getGuests} headline={"Guests"}></ListView>
	);
}

export default Guests;
