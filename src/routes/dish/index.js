import { h } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { useGlobals } from '../../providers/useGlobals';
import style from './style.css';
import { route } from 'preact-router';
import TextInput from '../../components/textInput/textInput';
import DetailsView from '../../components/detailsview';


// Note: `user` comes from the URL, courtesy of our router
const Dish = ({id}) => {
	const [dish, setDish] = useState({name:""})
	const [guests, setGuests] = useState([])
	const {saveDish, getDishes, getGuests} = useGlobals()
	useEffect(async () => {
		if(id){
			const numberId = Number.parseInt(id);
			const loadedDishes = await getDishes();
			const found = loadedDishes.find(curdish => curdish.id === numberId);
			if(found){
				setDish(found);
			}
		}
	}, [id])

	useEffect(async () => {
		const loadedGuests = await getGuests();
		if(!guests || guests.length != loadedGuests.length){
			setGuests(loadedGuests);
		}
	})

	const setRating = (guestId, value) => {
		if(!dish.ratings){
			dish.ratings = {}
		}
		dish.ratings[guestId] = Number.parseInt(value);
	}

	const isRating = (guestId, value)=> {
		if(!dish.ratings){
			dish.ratings = {}
		}
		return dish.ratings[guestId] == value
	}
	
	return (
		<DetailsView headline="Dish" onClick={() => { saveDish(dish); route("/dishes");}}>
			<TextInput label="Name" value={dish.name} onInput={(e) => {dish.name = e.target.value}}/>
			<TextInput label="Description" value={dish.description} onInput={(e) => {dish.description = e.target.value}}/>

			<h3>Ratings</h3>
			<div>
				{guests.map(guest => {
					return (
					<div>
						<h3>{guest.name}</h3>
						<div class={style.stars}>
							<input class={style.starInput+" "+style.starInput_5} id={"star-5"+guest.id} type="radio" name={"star"+guest.id} onInput={() => setRating(guest.id, 5)} checked={isRating(guest.id, 5)}/>
							<label class={style.starLabel+" "+style.starLabel_5} for={"star-5"+guest.id}></label>
							<input class={style.starInput+" "+style.starInput_4} id={"star-4"+guest.id} type="radio" name={"star"+guest.id} onInput={() => setRating(guest.id, 4)} checked={isRating(guest.id, 4)}/>
							<label class={style.starLabel+" "+style.starLabel_4} for={"star-4"+guest.id}></label>
							<input class={style.starInput+" "+style.starInput_3} id={"star-3"+guest.id} type="radio" name={"star"+guest.id} onInput={() => setRating(guest.id, 3)} checked={isRating(guest.id, 3)}/>
							<label class={style.starLabel+" "+style.starLabel_3} for={"star-3"+guest.id}></label>
							<input class={style.starInput+" "+style.starInput_2} id={"star-2"+guest.id} type="radio" name={"star"+guest.id} onInput={() => setRating(guest.id, 2)} checked={isRating(guest.id, 2)}/>
							<label class={style.starLabel+" "+style.starLabel_2} for={"star-2"+guest.id}></label>
							<input class={style.starInput+" "+style.starInput_1} id={"star-1"+guest.id} type="radio" name={"star"+guest.id} onInput={() => setRating(guest.id, 1)} checked={isRating(guest.id, 1)}/>
							<label class={style.starLabel+" "+style.starLabel_1} for={"star-1"+guest.id}></label>
							<input class={style.starInput+" "+style.starInput_0} id={"star-0"+guest.id} type="radio" name={"star"+guest.id} onInput={() => setRating(guest.id, 0)} checked={isRating(guest.id, 0)}/>
							<label class={style.starLabel+" "+style.starLabel_0} for={"star-0"+guest.id}></label></div>
					</div>
					)
				})}
			</div>
		</DetailsView>
	);
}

export default Dish;
