import { h } from 'preact';
import style from './style.css';
import { useGlobals } from '../../providers/useGlobals';
import DetailsView from '../../components/detailsview';
import { useEffect, useState } from 'preact/hooks';

const Settings = () => {

	const {importFromJson, exportToJson} = useGlobals()
	
	const downloadJson = (filename, data) => {
		var element = document.createElement('a');
		element.setAttribute('href', 
		'data:text/plain;charset=utf-8, '
		+ encodeURIComponent(data));
		element.setAttribute('download', filename);
	  
		// Above code is equivalent to
		// <a href="path of file" download="file name">
	  
		document.body.appendChild(element);
	  
		//onClick property
		element.click();
	}

	const uploadJson = async (e) => {
		const file = e.target.files[0];
		if (!file) {
		  return;
		}
		const reader = new FileReader();
		reader.onload = async function(e) {
		  const contents = e.target.result;
		  alert(contents)
		  await importFromJson(contents);
		};
		reader.readAsText(file);
	  }

	return <div class={style.home}>
		<h2>Settings</h2>
			<div class={style.gridContainer}>
				<h3>Data</h3><div></div>
				<button class={style.btn} onClick={async () => {
					const json = await exportToJson();
					downloadJson("export.json", json);
					}}>Export</button>
				<button class={style.btn} onClick={async () => {
					document.getElementById("fileInput").click();
					}}>Import</button>
				<input style="display:none" id="fileInput" type="file" onChange={uploadJson}>Import</input>
			</div>
			</div>

};

export default Settings;
