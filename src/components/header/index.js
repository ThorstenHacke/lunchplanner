import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style.css';
import { route } from 'preact-router';

const Header = () => {


	
	return <header class={style.header}>
		<button class={style.btn} onClick={() => route(`/settings`)}><i class="fa fa-gear"></i></button>

		<nav>
			<Link activeClassName={style.active} href="/">Home</Link>
			<Link activeClassName={style.active} href="/dishes">Dishes</Link>
			<Link activeClassName={style.active} href="/guests">Guests</Link>
		</nav>
	</header>
};

  

  
export default Header;
