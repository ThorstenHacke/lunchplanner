import { h } from 'preact';
import { Router } from 'preact-router';

import Header from './header';

// Code-splitting is automated for `routes` directory
import Home from '../routes/home';
import Dishes from '../routes/dishes';
import {ContextProvider} from '../providers/contextProvider'
import Dish from '../routes/dish';
import { createBrowserHistory } from 'history';
import Guests from '../routes/guests';
import Guest from '../routes/guest';
import Settings from '../routes/settings';


const App = () => {

	if (typeof window !== "undefined") { 
		const history = createBrowserHistory();
		
		const path = (/#!(\/.*)$/.exec(location.hash) || [])[1];
		if (path) {
			history.replace(path);
		}
	}
	return <div id="app">
		<ContextProvider>
			<Header />
			<Router>
				<Home path="/" />
				<Settings path="/settings" />
				<Dishes path="/dishes/" />
				<Dish path="/dish/:id" />
				<Dish path="/dish" />
				<Guests path="/guests/" />
				<Guest path="/guest/:id" />
				<Guest path="/guest" />
			</Router>
		</ContextProvider>
	</div>
}

export default App;
