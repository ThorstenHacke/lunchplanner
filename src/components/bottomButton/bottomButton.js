import { h } from 'preact';

import style from './style.css';
const BottomButton = ({text, onClick}) => {
	return <div class={style.bottom}>
				<button  onClick={onClick} >{text}</button>
			</div>
			
};
export default BottomButton;