import { h } from 'preact';
import style from './listview.css';
import { useEffect, useState } from 'preact/hooks';
import { Link } from 'preact-router/match';
import { route } from 'preact-router';

const ListView = ({headline, detailRoute, getItems, deleteItem}) => {
	const [items, setItems] = useState([]); 
	useEffect(async () => {
		const loadedItems = await getItems();
		if(!items || items.length !== loadedItems.length){
			setItems(loadedItems);
		}
	});
    return <div class={style.listView}>
		<h2>{headline}</h2>
		<div class={style.gridContainer}>
			{items && items.map(item => (
			<>
				<div>{item.name}</div>
				<button class={style.btn} onClick={() => route(`/${detailRoute}/${item.id}`)}><i class="fa fa-pencil"></i></button>
				<button class={style.btn} onClick={() => { deleteItem(item.id);setItems([])}}><i class="fa fa-trash"></i></button>
			</>
			))}
		</div>
		<button class={style.btn+" "+style.btnRight} onClick={() => route(`/${detailRoute}`)}><i class="fa fa-plus"></i></button>

</div>
}

export default ListView;