import { h } from 'preact';
import style from './detailsview.css';
import BottomButton from '../bottomButton/bottomButton';

const DetailsView = ({children, headline, onClick, buttonText="Save"}) => {
    console.log(children);
    return <div class={style.detailview}>
			<h2>{headline}</h2>
			{children}
			<BottomButton text={buttonText} onClick={onClick}/>
		</div>
}

export default DetailsView;