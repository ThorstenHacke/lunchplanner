
import style from './style.css';
const TextInput = ({label, value, onInput}) => {


	
	return <div class={style.inputWrapper}><span>{label}:</span><input type="text" value={value} onInput={onInput} /></div>
			
};

  

  
export default TextInput;