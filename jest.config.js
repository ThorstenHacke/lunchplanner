module.exports = {
    "preset": "jest-preset-preact",
    "setupFiles": [
      "<rootDir>/tests/__mocks__/browserMocks.js",
      "<rootDir>/tests/__mocks__/setupTests.js"
    ],
    "testMatch": [
      "<rootDir>/tests/*.(ts|tsx|mjs|js|jsx)"
    ],
    "collectCoverageFrom": ["src/**/*.js", "!**/node_modules/**"],
    "coverageReporters": ["html", "text", "text-summary", "cobertura"],
  }