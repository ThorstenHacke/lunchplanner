import { h } from 'preact';
import BottomButton from '../src/components/bottomButton/bottomButton';
// See: https://github.com/preactjs/enzyme-adapter-preact-pure
import { shallow } from 'enzyme';

describe('Initial Test of the BottomButton', () => {
	test('BottomButton renders 1 button', () => {
		const context = shallow(<BottomButton text="Hallo Welt" onClick={() => {}} />);
		expect(context.find('button').length).toBe(1);
	});
});