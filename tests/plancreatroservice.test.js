import * as plancreatorservice from "../src/services/plancreatorservice"

    const svenja = {id:"Svenja", weekdays:[true, true, true, true, true, true, true]};
    const thorsten = {id:"Thorsten", weekdays:[true, true, true, true, true, true, true]};
    const smilla = {id:"Smilla", weekdays:[false, true, true, false, true, true, true]};

    const bananen = {name:"Bananen", ratings:{"Svenja": 2, "Thorsten": 5, "Someone":5}};
    const birnen = {name:"Birnen", ratings:{"Svenja": 5, "Thorsten": 3}};
    const schokolade = {name:"Schokolade", ratings:{"Svenja": 5, "Thorsten": 5, "Smilla": 5}};
    const kokos = {name:"Kokos", ratings:{"Svenja": 5, "Thorsten": 0, "Smilla":0}};
    const brokkoli = {name:"Brokkoli", ratings:{"Svenja": 5, "Thorsten": 3, "Smilla":0}};
    const geleeBananen = {name:"Gelee Bananen", ratings:{"Svenja": 0, "Thorsten": 5}};
    const lachs = {name:"Lachs", ratings:{"Svenja": 4, "Thorsten": 4, "Smilla": 0}};
    const reis = {name:"Reis", ratings:{"Svenja": 2, "Thorsten": 3, "Smilla": 5}};
    const schupfnudeln = {name:"Schupfnudeln", ratings:{"Svenja": 3, "Thorsten": 4, "Smilla": 5}};
	test('create plan without inputs', () => {
        const actual = plancreatorservice.createPlan([], []);
        expect(actual).toStrictEqual([undefined,undefined,undefined,undefined,undefined,undefined,undefined]);
    })

    test('create plan with inputs', () => {
        const actual = plancreatorservice.createPlan([bananen, birnen, schokolade, brokkoli, lachs, reis, schupfnudeln], [svenja, smilla, thorsten]);
        expect(actual[0]).toBeOneOf([bananen, birnen, brokkoli, lachs]);
        expect(actual[1]).not.toBeOneOf([kokos, brokkoli, lachs]);
    })

    test('weekday guest filter', () => {

        expect(plancreatorservice.getGuestsOnWeekday([svenja, thorsten, smilla], 0)).toContainEqual(svenja);
        expect(plancreatorservice.getGuestsOnWeekday([svenja, thorsten, smilla], 0)).not.toContainEqual(smilla);
    })
    test('order dishes by guestranking', () => {

        const actual = plancreatorservice.getDishesOrderedByGuestRankings([bananen, schokolade, birnen], [svenja, thorsten]);
        expect(actual[0]).toStrictEqual(schokolade);
    })
    test.each([
        [schokolade, 10],
        [bananen, 7]
    ])('get dish total rank by guests', (input, expected) => {
        const actual = plancreatorservice.getDishTotalRank(input, [svenja, thorsten]);
        expect(actual).toBe(expected);
    })

    test('filter disliked dishes', () => {
        const actual = plancreatorservice.getDishesWithoutDislikes([bananen, birnen, schokolade, geleeBananen, kokos], [thorsten, svenja], []);
        expect(actual).not.toContainEqual(kokos);
        expect(actual).not.toContainEqual(geleeBananen);
        expect(actual).toContainEqual(bananen);
    })

    test('filter disliked and liked my other guests not participating', () => {
        const actual = plancreatorservice.getDishesWithoutDislikes([bananen, birnen, schokolade, geleeBananen, kokos, schupfnudeln], [thorsten, svenja], [smilla]);
        expect(actual).not.toContainEqual(kokos);
        expect(actual).not.toContainEqual(geleeBananen);
        expect(actual).not.toContainEqual(schupfnudeln);
        expect(actual).toContainEqual(bananen);
    })

    expect.extend({
        toBeOneOf(received, items) {
          const pass = items.indexOf(received) >= 0;
          const message = () =>
            `expected ${JSON.stringify(received)} to be contained in array [${JSON.stringify(items)}]`;
          if (pass) {
            return {
              message,
              pass: true
            };
          }
          return {
            message,
            pass: false
          };
        }
      });